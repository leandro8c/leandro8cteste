﻿app.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
          
            
            .state('home', {
                url: '/',
                templateUrl: 'pages/home.html',
                controller: 'homecontroller',
            })
            .state('listusers', {
                url: '/listusers',
                templateUrl: 'pages/listallusers.html',
                controller: 'userscontroller',
            })
            .state('userdetails', {
                url: '/userdetails',
                templateUrl: 'pages/userdetails.html',
                controller: 'userscontroller',
            })

            

        ;
    }]);
