﻿app.controller("userscontroller", function ($scope, $http) {
    $scope.since = 1;
    $scope.pagemessage = {};
    $scope.pagemessage.LastPage = 1;

    $scope.loadUsers = function () {
        $http.get('http://templateweb20180724122708.azurewebsites.net/api/user', { params: { since: $scope.since } }).then(function (data) {
            $scope.pagemessage = data.data;
            $scope.users = $scope.pagemessage.Data;
        });
    }

    $scope.nextPage = function () {
        
        $http.get('http://templateweb20180724122708.azurewebsites.net/api/user', { params: { since: $scope.pagemessage.NextPage } }).then(function (data) {
            var actualPage = $scope.pagemessage.ActualPage;
            $scope.pagemessage = data.data;
            $scope.pagemessage.LastPage = actualPage;
            $scope.users = $scope.pagemessage.Data;
        });
    }

    $scope.lastPage = function () {
        $http.get('http://templateweb20180724122708.azurewebsites.net/api/user', { params: { since: $scope.pagemessage.LastPage,foo: Date() } }).then(function (data) {
            $scope.pagemessage = data.data;
            $scope.users = $scope.pagemessage.Data;
        });
    }

    $scope.getUserDetails = function (name) {
        $http.get('http://templateweb20180724122708.azurewebsites.net/api/user/'+name+"/details" ).then(function (data) {
            $scope.user = data.data;
        });
        $http.get('http://templateweb20180724122708.azurewebsites.net/api/user/' + name + "/repos").then(function (data) {
            $scope.repos = data.data;
        });

    }

    $scope.clear = function () {
        $scope.user = null;
    }

});


