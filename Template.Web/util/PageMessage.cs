﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Template.Web.util
{
    //pagination class helper
    public class PageMessage
    {

        public Object Data { get; set; }
        public int NextPage { get; set; }
        public int LastPage { get; set; }
        public int ActualPage { get; set; }
    }
}
