﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Template.Web.util;
using Template.Web.Util;

namespace Template.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {


        // POST: User/Create
        [HttpGet]
        [Route("")]
        //receive frontend side request to get user list
        public async Task<ActionResult> users(int since)
        {
            try
            {
                // TODO: Add insert logic here
                var data = await ProcessUsers(since);
                return new ResultWithBody
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(data),
                };
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        [Route("{name}/repos")]
        //receive frontend side request to get user repositories
        public async Task<ActionResult> UserRepos(string name)
        {
            try
            {
                // TODO: Add insert logic here
                var data = await ProcessUserRepos(name);
                return new ResultWithBody
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Body = data,
                };
            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        [Route("{name}/details")]
        //receive frontend side request to get user details
        public async Task<ActionResult> UserDetails(string name)
        {
            try
            {
                var data = await ProcessUserDetails(name);
                return new ResultWithBody
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Body = data,
                };
            }
            catch
            {
                return View();
            }
        }


        //make a http request to get all users paginated (backend side).
        private static async Task<PageMessage> ProcessUsers(int pagenum)
        {


            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                //default credentials from microsoft
                client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
                var response = await client.GetAsync("https://api.github.com/users?since=" + pagenum);
                var link = response.Headers.GetValues("link");

                var page = new PageMessage();
                page.Data = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
                if (link != null)
                {
                    //threat the next page param from header link
                    var next = link.ElementAt(0).Split(";")[0];
                    int index = next.IndexOf("=");
                    if (index > 0)
                    {
                        var aux = next.Substring(index + 1, next.Length-index-2);
                        page.NextPage = int.Parse(aux);
                        page.ActualPage = pagenum;
                    }

                }

                return page;
            }
        }

        //make a http request to get user details (backend side).
        private static async Task<string> ProcessUserDetails(string name)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                //default credentials from microsoft
                client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
                var response = await client.GetAsync("https://api.github.com/users/" + name);
                return (await response.Content.ReadAsStringAsync());
            }
        }

        //make a http request to get the repositories from received user(backend side).
        private static async Task<string> ProcessUserRepos(string name)
        {

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                //default credentials from microsoft
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
                var response = await client.GetAsync("https://api.github.com/users/" + name + "/repos");
                return (await response.Content.ReadAsStringAsync());
            }
        }


    }
}